import serial.tools.list_ports


class PortConfigs:

    @staticmethod
    def get_ports():
        # A simple serial tool which will get all serially attached devices, then compare them to the
        # specific hardware ID of the XBee physical devices
        port_devices = serial.tools.list_ports.comports()
        matching_devices = []

        for port, device, hwid in port_devices:
            if hwid[12:21] == "0403:6015":
                matching_devices.append(port)
        return matching_devices
