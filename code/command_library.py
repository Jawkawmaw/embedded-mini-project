# This library is used to hold all commands that can be sent through the communication system

class CommandLibrary:

    def __init__(self, local_device, sensor):

        # Since we might need values from certain devices or access to sensors, objects are passed along
        self.local_device = local_device
        self.sensor = sensor
        self.commands = []

        # Saves all methods/functions in this class to a list as strings. That makes it easier to call them later and
        # so that more methods can be added without increased work in other places of the program
        # Original can be found here:
        # https://www.askpython.com/python/examples/find-all-methods-of-class
        # Modified by Jawkawmaw
        for function in dir(CommandLibrary):
            function_attribute_value = getattr(CommandLibrary, function)
            # Just to be sure, we check if the functions listed are actually callable
            if callable(function_attribute_value):
                # We remove built-in functions that is not specifically created in here
                if not function.startswith('__'):
                    self.commands.append(function)

    def request_node_id(self):
        # As the name says, this function will get the device name of whichever devices has received the request
        # and reply with said name
        return f"REPLY:{self.local_device.name}"

    def yip(self):
        # A simple ping method. Used to exclusive test connection.
        return f"REPLY:yap"

    def get_sensor_value(self):
        # This function will attempt to get the sensor value ONLY if the device is configured as an end-point.
        # If the device is not an end-point, it will ignore the command
        # In case the sensor isn't working or there's trouble getting data out, it would best to tell the user on the
        # other end instead of crashing the whole program
        try:
            if str(self.local_device.role) == 'Role.END_DEVICE':
                return f"REPLY:{self.sensor.get_sensor_reading()}"
            else:
                return f"REPLY:Device is not an end-point, ignoring command..."
        except:
            return f"REPLY:Something went wrong when attempting to get sensor readings. Check on physical device"


