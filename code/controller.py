import local_attached_devices
import communication
import ports
import discover
import sensor
import command_library
from sys import platform
import time


class Controller:

    def __init__(self):
        self.baud_rate = 9600
        self.platform = platform
        self.device_list = []
        self.port_finder = ports.PortConfigs()
        self.discover_functions = discover.Discover()
        self.number_of_devices = None
        self.device_ports = self.port_finder.get_ports()
        self.number_of_devices = len(self.device_ports)
        self.sensor = None
        # We only need a list of commands, not the whole library
        command_lib = command_library.CommandLibrary(None, None)
        self.commands_available = command_lib.commands
        del command_lib

        # Instantiate device object per device found, activate the callback with each one, then append the
        # communication system
        for port in self.device_ports:
            inst_device = local_attached_devices.AttachedLocalDevice(port, self.baud_rate)
            # if the device has the end-point role, instantiate the sensor as well
            if str(inst_device.local_device.get_role()) == 'Role.END_DEVICE' and \
                    not isinstance(self.sensor, sensor.fake_sensor):
                self.sensor = sensor.fake_sensor()
            communication_system = communication.CommunicationSystem(inst_device, self.sensor)
            inst_device.local_device.add_data_received_callback(communication_system.packet_received_callback)
            # The instantiated device and the communication system used by it will attached to the same list
            # then appended
            self.device_list.append([inst_device, communication_system])

    @staticmethod
    def retrieve_key_by_index(dictionary, index_to_find=0):
        # Since python 3.7 came out, dictionaries are now ordered, meaning that they can now be pseudo-indexed.
        # Unfortunately, no built-in method to retrieve a key by its index has been implemented yet, hence,
        # this function
        for dictionaryIndexNumber, key in enumerate(dictionary.keys()):
            if dictionaryIndexNumber == index_to_find:
                return key
        raise IndexError("Dictionary index out of range")

    @staticmethod
    def display_choices(list_to_display):
        # Takes a list and prep it for user display
        count = 1
        for option in list_to_display:
            print(f"{count}: {option}")
            count += 1

    def choice_input(self, choice_names):
        # This function is relying on the function above to work. Takes a list and makes sure the user input
        # is within the index of the list -1
        max_num_choice = len(choice_names)
        choice = -1
        while not 1 <= choice <= max_num_choice:
            self.display_choices(choice_names)
            try:
                choice = int(input("Choice: "))
            except:
                print("Input not recognized")
            if not 1 <= choice <= max_num_choice:
                print("Not a valid option, choose again")
        return choice

    @staticmethod
    def yes_no_loop():
        # A simply loop where the user cannot escape without using either y, n or pressing enter (enter is
        # the same as y). Returns true for y/enter and false for n
        user_input = None
        while not user_input == "y" and not user_input == "n":
            user_input = input("(y/n): ")
            if user_input == "y" or user_input == "":
                return True
            elif user_input == "n":
                return False

    def scan(self):
        # Assuming that device_list contains only device class objects
        # We iterate over it, and save any remote devices found per device
        # This means that each LOCAL device has a list of the REMOTE devices it can see
        found_something = 0
        for device in self.device_list:
            print(f"Scanning for devices using: {device[0].get_name()}")
            device[0].set_known_devices(self.discover_functions.discover_devices(device[0].get_local_device(), True))
            if device[0].known_devices:
                found_something += 1
        if found_something == 0:
            return False
        else:
            return True

    @staticmethod
    def now_ms():  # Find current time and return it in milliseconds, written by NISI
        ms = int(time.time() * 1000)
        return ms

    def await_response(self, sending_node_comm):
        # Queries the new_packet value in a given communication_system object until it is True.
        # If the loop time is exceeded, and the value is still False, will continue with no return
        # If the value is however True, it will instead pop the deque, take the first index of the list, and
        # remove the REPLY: prefix, then return it
        start_time = self.now_ms()
        loop_duration = 10000
        while sending_node_comm.new_packet is False and (self.now_ms() - start_time > loop_duration) is False:
            time.sleep(0.1)
        if sending_node_comm.new_packet is False:
            print("No response was received in time. Assuming failure. Returning...\n")
        else:
            sending_node_comm.new_packet = False
            return sending_node_comm.packet_buffer.pop()[1][6:]

    @staticmethod
    def manual_mode():
        # Simple block to prevent things that should not be running from running
        # In future versions, an idea would be to program it to not allow a user to continue
        # unless an end-device was detected
        print("Welcome! Starting up...\n"
              "The program will run in response mode until you wish to escape this mode.\n"
              "The system will still respond to commands it receives while in manual.\n"
              "When exiting response mode, a scan will be initiated on all devices attached\n"
              "Please any key to continue\n")
        input()

    def begin(self):

        # Block until input
        self.manual_mode()

        # Scan first, will stay in loop until something is found or program is exited
        scan_result = self.scan()
        if not scan_result:
            while not scan_result:
                print("Found nothing on any locally connected devices... :(\n"
                      "Do you wish to try again?\n")
                if self.yes_no_loop():
                    scan_result = self.scan()
                else:
                    exit(5)

        while True:
            # If more than one device is attached, the user will have to option to choose which
            # device they want to use. Since our list starts at index 0, we use -1 at the end.
            if self.number_of_devices > 1:
                print("Multiple local devices found, which device do you wish to send from?\n")
                # Get the name from all devices attached and send it to be prepped display, and so that the
                # user can choose which device to use
                s_choice = self.choice_input([device[0].get_name() for device in self.device_list])
                # list inside a list, first is the combined device, then the device object. Same principle
                # below, except with the comm system
                sending_node = self.device_list[s_choice - 1][0]
                sending_node_comm = self.device_list[s_choice - 1][1]
            else:
                # If there's not more than one device, take the first on the list
                sending_node = self.device_list[0][0]
                sending_node_comm = self.device_list[0][1]
            sending_node_address = sending_node.get_address()

            print("\nDiscovery has already run once.\n"
                  "Do you wish to proceed? If not, a scan will be performed \n"
                  "again to check for new devices\n")
            if self.yes_no_loop() is False:
                self.scan()

            # Prep remote a way to display remote devices
            r_device_name_list = []
            # The known_devices dict carries a list as the value. First index is remote device object, the
            # the second index is for the name
            for key, value in sending_node.get_known_devices().items():
                if value[1] is None:
                    # If name is unknown, we request it via a quick packet craft and send it off
                    temp_packet = sending_node_comm.craft_packet(sending_node_address, "request_node_id")
                    sending_node_comm.send_packet(value[0], temp_packet)
                    print("Names for devices are unknown, fetching...\n")
                    # Wait for a packet to return using await_response
                    temp_node_id = self.await_response(sending_node_comm)
                    if temp_node_id is not None:
                        # append the name to it's place on the list
                        sending_node.known_devices[key] = [value[0], temp_node_id]
                        r_device_name_list.append(temp_node_id)
                    else:
                        print("Error, no name was returned. Only 64bit addresses will be available\n")
                        r_device_name_list.append(key)
                else:
                    # If names are already known, simply append them
                    r_device_name_list.append(value[1])

            print("Which device do you wish to send to?\n")
            r_choice = self.choice_input(r_device_name_list)
            # retrieve node address by dict index
            receiving_node_address = self.retrieve_key_by_index(sending_node.get_known_devices(), r_choice - 1)
            receiving_node = sending_node.get_known_devices().get(receiving_node_address)[0]

            print("\nWhat do you wish to send?\n")
            # Display a list of commands the user can send to the selected device
            c_choice = self.choice_input(self.commands_available)
            command = self.commands_available[c_choice-1]
            # Craft the packet with the command, along with the node/device address it came from
            packet = sending_node_comm.craft_packet(sending_node_address, command)
            sending_node_comm.send_packet(receiving_node, packet)

            print("Awaiting response...")
            response = self.await_response(sending_node_comm)
            print(f"RESPONSE RECEIVED:\n\n{response}")
            time.sleep(3)
