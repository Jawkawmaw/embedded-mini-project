# Responsible for all communication between 1 locally attached device and the rest of the network.
# Each local device should have 1 CommunicationSystem attached to it

import collections
from digi.xbee.devices import RemoteXBeeDevice
from digi.xbee.models.address import XBee64BitAddress
import command_library


class CommunicationSystem:

    def __init__(self, local_device, sensor):
        # local device and sensor are passed on to be used elsewhere
        self.local_device = local_device
        self.sensor = sensor
        # Create buffers to hold responses. Circular so we don't cause memory leak if the device is operating for years
        self.packet_buffer = collections.deque(maxlen=50)
        self.new_packet = False
        self.command_lib = command_library.CommandLibrary(local_device, sensor)
        self.commands_available = self.command_lib.commands

    def execute_function(self, command_list, command_received):
        # This function is responsible for matching the commands that the device receives with the commands that
        # are available
        for command in command_list:
            if command == command_received:
                function = getattr(self.command_lib, command_received)
                return function()

    def packet_received_callback(self, zigbee_message):
        # Callback function responsible for handling packets that arrives
        # Runs in it's own thread
        data = zigbee_message.data.decode("utf8")

        # Split message up so we can work with it
        source_address = data[0:16]
        command = data[19:]

        # If the package contains the prefix REPLY:, then that means it goes into the buffer. The new_packet
        # value is then set to True so that the controller can pop the new packet
        if command[0:6] == 'REPLY:':
            self.packet_buffer.append([source_address, command])
            self.new_packet = True
        else:
            reply = self.execute_function(self.commands_available, command)
            if reply is None:
                print("Error, selected function was not valid, ignoring...")
            else:
                # If the incoming packet is not a reply, an answer is constructed and sent
                reply_packet = self.craft_packet(self.local_device.address, reply)
                remote_node = RemoteXBeeDevice(self.local_device.local_device,
                                               XBee64BitAddress.from_hex_string(source_address))
                self.send_packet(remote_node, reply_packet)

    def send_packet(self, remote_node, packet):
        self.local_device.local_device.send_data(remote_node, packet)

    @staticmethod
    def craft_packet(source64bit, command):
        return f"{source64bit} - {command}"

    # def craft_packet(self, source64bit, destination64bit, command):
    #    return f"{source64bit} - {destination64bit} - {command}"

    # def packet_received_callback(self, zigbeeMessage): # FUUUUCK CAN'T ADD AN EXTRA ARGUMENT
    #    data = zigbeeMessage.data.decode("utf8")
    #
    #    # Split message up so we can work with it
    #    sourceAddress = data[0:16]
    #    destinationAddress = data[19:35]
    #    command = data[38:]
    #
    #    # Will check if message is for this device first, and if not, check if device is coordinator
    #    if destinationAddress == localDevice.get_64bit_addr():
    #        if command[0:7] == "NODE ID:":
    #            self.name_buffer.append(command)
    #            self.new_name_packet = True
    #        else:
    #            self.packet_buffer.append(command)
    #    elif not destinationAddress == localDevice.get_64bit_addr() and localDevice.get_role() == 'Role.COORDINATOR':
    #        self.packet_routing(localDevice, sourceAddress, destinationAddress, command)
    #
    #
    # def packet_routing(self, localDevice, sourceAddress, destinationAddress, command):
    #    packet_forward_success = False
    #    # Forwards message if destination is known and the localDevice has the coordinator role, CHECK UP GET_ROLE
    #
    #    for knownDeviceAddress, remoteDeviceObject in localDevice.get_known_devices():
    #        if knownDeviceAddress == destinationAddress:
    #            self.send_packet(localDevice, remoteDeviceObject, self.craft_packet(sourceAddress, destinationAddress, command))
    #            packet_forward_success = True
    #            print("Communication: packet forwarded")
    #            break
    #    if not packet_forward_success:
    #        for knownDeviceAddress, remoteDevice in localDevice.get_known_devices():
    #            if knownDeviceAddress == sourceAddress:
    #                error_message = "Failure to deliver"
    #                self.send_packet(localDevice, remoteDevice, self.craft_packet(sourceAddress, destinationAddress, error_message))
    #                print("Communication: error, destination unknown, returned failure packet")
    #                break

    #### BROADCAST??? ####
    #### MISSING FUNCTION/ABILITY TO HANDLE PACKETS WHERE ONLY THE NAME IS KNOWN ###
