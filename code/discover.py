# Discovers devices that the locally attached device can see, then saves them in a dict with their address as key
# and their instantiated class as the value
from digi.xbee.devices import RemoteXBeeDevice
from digi.xbee.models.address import XBee64BitAddress
from digi.xbee.models.options import DiscoveryOptions
from time import sleep


class Discover:

    @staticmethod
    def discover_devices(local_device, discover_self_option):
        raw802_network = local_device.get_network()
        raw802_network.set_discovery_timeout(4)

        # Code right below has no effect given bug mentioned here:
        # https://github.com/digidotcom/xbee-python/issues/245
        # Still kept for demonstrational purposes
        if discover_self_option:
            raw802_network.set_discovery_options({DiscoveryOptions.DISCOVER_MYSELF})

        # raw802_network.add_device_discovered_callback(self.add_device_callback)

        raw802_network.start_discovery_process()
        # Wait until the discovery is finished
        while raw802_network.is_discovery_running():
            sleep(0.1)

        devices = raw802_network.get_devices()
        temp_device_dictionary = {}
        for device in devices:
            # The device must be casted as a string to be manipulated
            stringed_device = str(device)
            combi_list = []
            # The stringed_device[0:16] is used because that's where the hex values are
            combi_list.append(RemoteXBeeDevice(local_device, XBee64BitAddress.from_hex_string(stringed_device[0:16])))
            combi_list.append(combi_list[0].get_node_id())
            temp_device_dictionary[stringed_device[0:16]] = combi_list

        return temp_device_dictionary



    @staticmethod
    def add_device_callback(discovered_device):
        stringed_device = str(discovered_device)
        print(stringed_device)
        # tempDeviceDictionary[stringed_device[0:16]] = RemoteXBeeDevice(local_device, XBee64BitAddress.from_hex_string(stringed_device[0:16]))
