# This class instantiates a local device and immediately sets it up for usage
from digi.xbee.devices import XBeeDevice


class AttachedLocalDevice:
    def __init__(self, port, baud_rate):
        self.port = port
        self.baudRate = baud_rate
        self.local_device = XBeeDevice(port, baud_rate)
        self.local_device.open()
        self.name = self.local_device.get_node_id()
        self.known_devices = {}
        self.address = self.local_device.get_64bit_addr()
        self.role = self.local_device.get_role()

    # While not used, allows a manual setting of the known devices list
    def set_known_devices(self, dictionary):
        self.known_devices = dictionary

    def get_known_devices(self):
        return self.known_devices

    def get_local_device(self):
        return self.local_device

    def get_name(self):
        return self.name

    def get_address(self):
        return self.address
